import chess
import random
import math

A_VERY_BIG_INTEGER = 1000000
A_VERY_SMALL_INTEGER = -1000000
PIECE_SCORE = {
    1 : 1,
    2 : 3,
    3 : 3,
    4 : 5,
    5 : 9,
    6 : 0
}

def i_win(board, my_colour):
    if board.is_checkmate():
        return my_colour != board.turn
    return False

def i_lose(board, my_colour):
    return i_win(board, not my_colour)

def net_material_score(board, my_colour):
    nms = 0
    for p in board.piece_map().values():
        m = 1 if p.color == my_colour else -1
        nms += m * PIECE_SCORE[p.piece_type]
    return nms

def score_position(board, my_colour, search_depth):
    if board.is_game_over():
        if i_win(board, my_colour):
            return A_VERY_BIG_INTEGER*math.pow(10,search_depth), 1
        else:
            return A_VERY_SMALL_INTEGER*math.pow(10,search_depth), 1
    else:
        if search_depth > 0:
            combined_score = 0
            total_leaves = 0
            for move, score, leaves in get_scored_moves(board, my_colour, search_depth-1):
                combined_score += score*math.pow(10,search_depth)
                total_leaves += leaves
            return combined_score, total_leaves
        else:
            nms = net_material_score(board, my_colour)
            return nms, 1

def get_scored_moves(board, my_colour, search_depth):
    for move in board.legal_moves:
        if not board.is_castling(move) and not board.is_en_passant(move):
            board.push(move)
            score, leaves = score_position(board, my_colour, search_depth-1)
            yield move, score, leaves
            board.pop()


def my_turn(board):
    chosen_moves = []
    best_score = A_VERY_SMALL_INTEGER
    leaves_evaluated = 0
    for move, score, leaves in get_scored_moves(board, board.turn, 5):
        print(move, score, leaves)
        leaves_evaluated += leaves
        if score > best_score or len(chosen_moves) == 0:
            chosen_moves = [move]
            best_score = score
        elif score == best_score:
            chosen_moves.append(move)
    move = random.choice(chosen_moves)
    print(move, best_score, leaves_evaluated)
    board.push(move)


def opponent_turn(board):
    while True:
        try:
            my_move_str = input("What is your move?: ")
            my_move = chess.Move.from_uci(my_move_str)
            if my_move in board.legal_moves:
                board.push(my_move)
                return
            else:
                raise ValueError()
        except ValueError:
            print("Invalid Move, try again")

def game_loop(board, my_colour):
    while not board.is_game_over():
        if board.turn == my_colour:
            my_turn(board)
        else:
            opponent_turn(board)

if __name__ == "__main__":
    game_loop(chess.Board(), chess.WHITE)
